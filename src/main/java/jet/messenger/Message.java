package jet.messenger;

/**
 * Message contents message fields
 */
public class Message {
    private String recieverUserId;
    private String messageBody;

    public String getRecieverUserId() {
        return recieverUserId;
    }

    public void setRecieverUserId(String recieverUserId) {
        this.recieverUserId = recieverUserId;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
