package jet.messenger;

/**
 * Contains user information
 */
public class User {
    private String userID;
    private String[] userList;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }


    public String[] getUserList() {
        return userList;
    }

    public void setUserList(String[] userList) {
        this.userList = userList;
    }

}
